### Hoe Warm Is Het In Delft

This phrase means how is the weather in Delft.

Delft (Dutch pronunciation: [ˈdɛl(ə)ft]) is a city and municipality in the province of South Holland, Netherlands.
It is located between Rotterdam, to the southeast, and The Hague, to the northwest. Together with them, it is a 
part of both the Rotterdam–The Hague metropolitan area and the Randstad.
