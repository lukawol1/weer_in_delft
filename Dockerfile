FROM python:3.9.19-alpine

WORKDIR /weer_in_delft

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY ./weer_in_delft ./weer_in_delft

CMD ["python", "weer_in_delft/HoeWarmIsHetInDelft.py"]
