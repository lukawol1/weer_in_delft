from http import HTTPStatus
from typing import Any, Generator, List
from unittest import mock

import pytest
import requests

from weer_in_delft.HoeWarmIsHetInDelft import (
    SOURCES_COMPILIANCE_CHECK,
    TEMPERATURE_INDEX,
    get_temperature,
    make_get_request,
    validate_compiliance,
    validate_data,
)


@pytest.fixture(name="raw_data")
def raw_data_fixture() -> str:
    return (
        "12345 0.0 0.0 0 0.0 0 0.0 0.0 0.0 0.0 0.00 0.00 0.0 0 100.0 1 0.0 0 0 0.0 "
        "-100.0 -100.0 -100.0 -100.0 -100.0 -100.0 -100 -100 -100 05 43 13 weerindelft_-5:43:13_AM 0 0 6 "
        "6 100 100 100 100 100 100 100 0.0 -2.2 -100.0 100.0 1 Dry/Night_time 0.0 0 0 0 0 0 0 0 0 0 0 0 0 "
        "0 0 0 0 0 0 0 0 0.0 0.0 0.0 6/6/2024 -2.2 -2.2 -100.0 150.0 0.0 0 0 0 0 0 0 0 0 0 0 0.0 0.0 0.0 "
        "0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 -100.0 100.0 0.0 0.0 0 --- "
        "--- 0 0 0 -100.0 -100.0 -100 -100 -100 -100 -100 0.0 -20.0 50.0 -1.5 0.0 1500.0 0 5:00_AM "
        "10:40:12 -1.5 -1.5 -100.0 100.0 0 2024 0.0 -1 0 0 -0 -0 -0 -0 -0 -0 -0 -0 -0 -0 0.0 255.0 0.0 "
        "0.0 0.00000 0.00000 0.0 0 101 0.0  0.0 0.0 0.0 0.0 0.0 0.0 0.0   0 !!C10.37S143!! "
    )


@pytest.fixture(name="preprocessed_data")
def preprocessed_data_fixture(raw_data: str) -> List[str]:
    return raw_data.split()


@pytest.fixture(name="mocked_hashlib")
def hashlib_mock() -> Generator[Any, None, None]:
    class HashlibMock:  # pylint: disable=too-few-public-methods
        hashes_to_return = list(SOURCES_COMPILIANCE_CHECK.values())
        algorithms_available = ["sha256"]
        times_called = 0

        def sha256(self, _: Any) -> mock.Mock:
            hash_mock = mock.Mock()
            hash_mock.hexdigest.return_value = self.hashes_to_return[self.times_called]
            self.times_called += 1

            return hash_mock

    mocked_hashlib = HashlibMock()
    with mock.patch("weer_in_delft.HoeWarmIsHetInDelft.hashlib", new=mocked_hashlib):
        yield mocked_hashlib


@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.debug")
@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.warning")
def test_make_get_request_success(
    logging_warning_mock: mock.Mock, logging_debug_mock: mock.Mock
) -> None:
    url = "https://www.test.url/mocked"
    headers = {"Accept": "*/*"}
    with mock.patch("weer_in_delft.HoeWarmIsHetInDelft.requests.get") as get_mock:
        resp_mock = mock.Mock()
        resp_mock.status_code = HTTPStatus.OK
        resp_mock.text = "response text"
        get_mock.return_value = resp_mock

        resp = make_get_request(url, headers=headers)

        get_mock.assert_called_once_with(url, headers=headers, timeout=30)
        assert resp.text == "response text"
        logging_debug_mock.assert_not_called()
        logging_warning_mock.assert_not_called()


@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.debug")
@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.warning")
def test_make_get_request_error_code(
    logging_warning_mock: mock.Mock, logging_debug_mock: mock.Mock
) -> None:
    url = "https://www.test.url/mocked"
    headers = {"Accept": "*/*"}
    with mock.patch("weer_in_delft.HoeWarmIsHetInDelft.requests.get") as get_mock:
        resp_mock = mock.Mock()
        resp_mock.status_code = HTTPStatus.NOT_FOUND
        resp_mock.text = "Not Found"
        get_mock.return_value = resp_mock

        with pytest.raises(ValueError):
            make_get_request(url, headers=headers)

        get_mock.assert_called_once_with(url, headers=headers, timeout=30)
        logging_warning_mock.assert_called_once_with(
            f"Unexpected status code (404) of response from {url}"
        )
        logging_debug_mock.assert_called_once_with("Content of 404 response: Not Found")


@pytest.mark.parametrize(
    "exc,log",
    [
        (requests.Timeout(), "GET on {url} timeouted (timeout set to 30)"),
        (requests.ConnectionError(), "ConnectionError occured during GET on {url}"),
        (requests.RequestException(), "Exception occured during GET on {url}"),
    ],
)
@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.debug")
@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.warning")
def test_make_get_request_exception(
    logging_warning_mock: mock.Mock, logging_debug_mock: mock.Mock, exc: Exception, log: str
) -> None:
    url = "https://www.test.url/mocked"
    headers = {"Accept": "*/*"}
    with mock.patch("weer_in_delft.HoeWarmIsHetInDelft.requests.get") as get_mock:
        get_mock.side_effect = exc

        with pytest.raises(type(exc)):
            make_get_request(url, headers=headers)

        get_mock.assert_called_once_with(url, headers=headers, timeout=30)
        logging_warning_mock.assert_called_once_with(log.format(url=url))
        logging_debug_mock.assert_not_called()


def test_validate_data_success(preprocessed_data: List[str]) -> None:
    assert len(preprocessed_data) == 175

    validate_data(preprocessed_data)


def test_validate_data_wrong_length(preprocessed_data: List[str]) -> None:
    data = preprocessed_data[:15] + preprocessed_data[-3:]

    with pytest.raises(ValueError) as ve:
        validate_data(data)

        assert ve.value == f"unexpected number of items in weather data array ({len(data)})"


def test_validate_data_no_start_token(preprocessed_data: List[str]) -> None:
    preprocessed_data[0] = "a"

    with pytest.raises(ValueError) as ve:
        validate_data(preprocessed_data)

        assert ve.value == "unexpected format of weather data"


def test_validate_data_invalid_version_at_end(preprocessed_data: List[str]) -> None:
    preprocessed_data[-1] = "a"

    with pytest.raises(ValueError) as ve:
        validate_data(preprocessed_data)

        assert ve.value == "unexpected format of weather data"


@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.info")
@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.warning")
@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.error")
def test_validate_compiliance_success(
    logging_error_mock: mock.Mock,
    logging_warning_mock: mock.Mock,
    logging_info_mock: mock.Mock,
    mocked_hashlib: Any,
) -> None:
    with mock.patch("weer_in_delft.HoeWarmIsHetInDelft.make_get_request") as make_get_request_mock:
        validate_compiliance()

        assert mocked_hashlib.times_called == 3
        assert make_get_request_mock.call_count == 3
        logging_info_mock.assert_called_once_with("Confirmed compiliance of all checked sources")
        logging_warning_mock.assert_not_called()
        logging_error_mock.assert_not_called()


@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.info")
@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.warning")
@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.error")
def test_validate_compiliance_no_hashing_algorithm(
    logging_error_mock: mock.Mock,
    logging_warning_mock: mock.Mock,
    logging_info_mock: mock.Mock,
    mocked_hashlib: Any,
) -> None:
    mocked_hashlib.algorithms_available = ["md5"]
    validate_compiliance()

    assert mocked_hashlib.times_called == 0
    logging_info_mock.assert_not_called()
    logging_warning_mock.assert_called_with(
        "Hashing algorithm necessary to confirm compiliance of sources not available, skipping"
    )
    logging_error_mock.assert_not_called()


@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.info")
@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.warning")
@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.error")
def test_validate_compiliance_changed_hash(
    logging_error_mock: mock.Mock,
    logging_warning_mock: mock.Mock,
    logging_info_mock: mock.Mock,
    mocked_hashlib: Any,
) -> None:
    mocked_hashlib.hashes_to_return[1] = "changed"
    with mock.patch("weer_in_delft.HoeWarmIsHetInDelft.make_get_request") as make_get_request_mock:
        validate_compiliance()

        assert mocked_hashlib.times_called == 2
        assert make_get_request_mock.call_count == 2
        logging_info_mock.assert_not_called()
        logging_warning_mock.assert_not_called()
        logging_error_mock.assert_called_once_with(
            "Detected changes in weerindelft sources which could affect result of this program. Correctnes is no longer guaranteed"
        )


@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.info")
@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.warning")
@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.error")
def test_validate_compiliance_error_on_request(
    logging_error_mock: mock.Mock,
    logging_warning_mock: mock.Mock,
    logging_info_mock: mock.Mock,
    mocked_hashlib: Any,
) -> None:
    mocked_hashlib.hashes_to_return[1] = mocked_hashlib.hashes_to_return[2]

    def get_req_side_effect(url: str) -> mock.Mock:
        if url == "https://weerindelft.nl/WU/55ajax-dashboard-testpage.php":
            raise ValueError()
        return mock.Mock()

    with mock.patch("weer_in_delft.HoeWarmIsHetInDelft.make_get_request") as make_get_request_mock:
        make_get_request_mock.side_effect = get_req_side_effect

        validate_compiliance()

        assert mocked_hashlib.times_called == 2
        assert make_get_request_mock.call_count == 3
        logging_info_mock.assert_not_called()
        logging_warning_mock.assert_called_once_with(
            "Failed to get source of https://weerindelft.nl/WU/55ajax-dashboard-testpage.php, skipping checking hash"
        )
        logging_error_mock.assert_not_called()


@pytest.mark.parametrize(
    "temp_str, expected_temp", [("0.0", 0.0), ("17.3", 17.0), ("22.7", 23.0), ("-1.8", -2.0)]
)
@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.error")
def test_get_temperature_success(
    logging_error_mock: mock.Mock, temp_str: str, expected_temp: float, raw_data: str
) -> None:
    data = raw_data.split()
    data[TEMPERATURE_INDEX] = temp_str
    raw_data = " ".join(data)
    response_mock = mock.Mock()
    response_mock.text = raw_data

    with mock.patch("weer_in_delft.HoeWarmIsHetInDelft.make_get_request") as get_mock, mock.patch(
        "weer_in_delft.HoeWarmIsHetInDelft.validate_data"
    ):
        get_mock.return_value = response_mock

        parsed_temp = get_temperature()

        assert parsed_temp == expected_temp
        logging_error_mock.assert_not_called()
        get_mock.assert_called_once()


@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.error")
def test_get_temperature_invalid_value(logging_error_mock: mock.Mock, raw_data: str) -> None:
    data = raw_data.split()
    data[TEMPERATURE_INDEX] = "minus_one"
    raw_data = " ".join(data)
    response_mock = mock.Mock()
    response_mock.text = raw_data

    with mock.patch("weer_in_delft.HoeWarmIsHetInDelft.make_get_request") as get_mock, mock.patch(
        "weer_in_delft.HoeWarmIsHetInDelft.validate_data"
    ):
        get_mock.return_value = response_mock

        with pytest.raises(ValueError):
            get_temperature()

        logging_error_mock.assert_called_once_with("Unable to parse temperature")
        get_mock.assert_called_once()


@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.error")
def test_get_temperature_invalid_data(logging_error_mock: mock.Mock, raw_data: str) -> None:
    response_mock = mock.Mock()
    response_mock.text = raw_data

    with mock.patch("weer_in_delft.HoeWarmIsHetInDelft.make_get_request") as get_mock, mock.patch(
        "weer_in_delft.HoeWarmIsHetInDelft.validate_data"
    ) as validate_data_mock:
        get_mock.return_value = response_mock
        validate_data_mock.side_effect = ValueError("unexpected format of weather data")

        with pytest.raises(ValueError):
            get_temperature()

        logging_error_mock.assert_called_once_with(
            "Invalid weather data unexpected format of weather data"
        )
        get_mock.assert_called_once()


@mock.patch("weer_in_delft.HoeWarmIsHetInDelft.logging.error")
def test_get_temperature_failed_get(logging_error_mock: mock.Mock) -> None:

    with mock.patch("weer_in_delft.HoeWarmIsHetInDelft.make_get_request") as get_mock:
        get_mock.side_effect = ValueError()

        with pytest.raises(ValueError):
            get_temperature()

        logging_error_mock.assert_called_once_with("Failed to fetch weather data")
        get_mock.assert_called_once()
