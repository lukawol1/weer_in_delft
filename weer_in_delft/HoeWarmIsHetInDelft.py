import argparse
import hashlib
import logging
import re
import sys
import time
from http import HTTPStatus
from typing import Any, Dict, List, Optional

import requests

SOURCES_COMPILIANCE_CHECK = {
    "https://www.weerindelft.nl": "8c6e8fcc062f3d92aa9749e7027de9105e86f513fb6535af706c19a2ff38f6ee",
    "https://weerindelft.nl/WU/55ajax-dashboard-testpage.php": "b704743571dc883b720dcd74709bff58b02a758b98efb858e72cee98eaedca23",
    "https://weerindelft.nl/WU/ajaxWDwx.js": "a3c490ad24eb103f85518cb03ce10ced1522397d0e6e0db20b9d7738e78c1bab",
}
WEATHER_DATA_URL = "https://www.weerindelft.nl/clientraw.txt"
# below values are from ajaxWDwx.js code
VERSION_REGEX = r"!![a-zA-Z0-9]+\.[a-zA-Z0-9]+!!"  # this is diffrent from re in website source because original one was not correct
START_TOKEN = "12345"
DATA_LEN = 175  # I checked length of items in response with weather data
TEMPERATURE_INDEX = 4
UNIT_SIGN = "\u2103" if str(sys.stdout.encoding).lower().startswith("utf") else " C"


def get_args() -> Any:
    parser = argparse.ArgumentParser(
        prog="Hoe Warm Is In Delft",
        description="Program purpose is to fetch, parse and output on console current temperature in Delft NL",
    )
    parser.add_argument("-v", "--verbose", action="store_true", help="More logs")
    parser.add_argument(
        "-i",
        "--ignore-compiliance",
        action="store_true",
        help="Do not check for changes in sources of weerindelft",
    )
    return parser.parse_args()


def make_get_request(
    url: str, headers: Optional[Dict[str, str]] = None, timeout: int = 30
) -> requests.Response:
    if headers is None:
        headers = {}
    try:
        response = requests.get(url, headers=headers, timeout=timeout)
    except requests.Timeout:
        logging.warning(f"GET on {url} timeouted (timeout set to {timeout})")
        raise
    except requests.ConnectionError:
        logging.warning(f"ConnectionError occured during GET on {url}")
        raise
    except requests.RequestException:
        logging.warning(f"Exception occured during GET on {url}")
        raise

    if response.status_code != HTTPStatus.OK:
        logging.warning(f"Unexpected status code ({response.status_code}) of response from {url}")
        logging.debug(f"Content of {response.status_code} response: {response.text}")
        raise ValueError()

    return response


def validate_data(items: List[str]) -> None:
    if len(items) != DATA_LEN:
        raise ValueError(f"unexpected number of items in weather data array ({len(items)}")
    if items[0] != START_TOKEN or re.fullmatch(VERSION_REGEX, items[-1]) is None:
        raise ValueError("unexpected format of weather data")


def validate_compiliance() -> None:
    if "sha256" not in hashlib.algorithms_available:
        logging.warning(
            "Hashing algorithm necessary to confirm compiliance of sources not available, skipping"
        )
        return

    skipped = False
    for url, expected_hash in SOURCES_COMPILIANCE_CHECK.items():
        try:
            response = make_get_request(url)
        except (requests.RequestException, ValueError):
            logging.warning(f"Failed to get source of {url}, skipping checking hash")
            skipped = True
            continue
        if hashlib.sha256(response.content).hexdigest() != expected_hash:
            logging.error(
                "Detected changes in weerindelft sources which could affect result of this program. Correctnes is no longer guaranteed"
            )
            return
    if not skipped:
        logging.info("Confirmed compiliance of all checked sources")


def get_temperature() -> float:
    try:
        response = make_get_request(
            WEATHER_DATA_URL + f"?{int(time.time())}",
            headers={
                "Accept": "*/*",
                "Accept-Encoding": "gzip, deflate, br, zstd",
                "Accept-Language": "en-US,en,q=0.7",
                "Referer": "https://weerindelft.nl/WU/55ajax-dashboard-testpage.php",
            },
        )
    except (requests.RequestException, ValueError):
        logging.error("Failed to fetch weather data")
        raise ValueError from None

    data = response.text.split()
    try:
        validate_data(data)
    except ValueError as e:
        logging.error(f"Invalid weather data {str(e)}")
        raise

    try:
        temp = round(float(data[TEMPERATURE_INDEX]))
    except Exception:
        logging.error("Unable to parse temperature")
        raise ValueError() from None

    return temp


def main() -> None:
    args = get_args()
    logging.basicConfig(
        format="%(asctime)s|%(levelname)s %(message)s",
        level=logging.DEBUG if args.verbose else logging.WARNING,
    )
    if not args.ignore_compiliance:
        validate_compiliance()
    try:
        temp = get_temperature()
        print(f"Current temperature in Delft is {round(temp)}{UNIT_SIGN}")
    except ValueError:
        pass


if __name__ == "__main__":
    main()
